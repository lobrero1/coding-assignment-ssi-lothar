In case of High Memory usage:
    When there are high memory consuming processes, the system can be left out without memory to use, forcing it to use swap memory. The information on the swap 
    is stored in the disk and it will affect the performance of the service and the server itself since its a lot slower to access information stored in swap memory.
    This may cause Application bottlenecks, affecting the health of the system.
