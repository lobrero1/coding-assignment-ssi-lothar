require 'net/ssh'
require 'logger'
require 'optparse'
require 'yaml'
require 'ostruct'
require 'timeout'


$logger = Logger.new("monitoring-logs.log")
$logger.datetime_format = "%Y-%m-%d %H:%M:%S"
$logger.level = Logger::INFO

#Class to load Config file.
#Also exits since file not found
class LoadVariables
    def initialize (filename)
        begin
            connection_options = YAML.load_file(filename)
            $USER=connection_options['username']
            $KEY=connection_options['sshkey_location']
            $logger.info {"Username loaded: " + $USER }
            $logger.info {"SSH key location loaded: " + $KEY }
        rescue Errno::ENOENT
            puts "No config " + filename +  " file found"
            exit
        end
    end
end

class Optparse  
    #
    # Class to get parameters when executing the scritp.
    #
    def self.parse(args)
      # The options specified on the command line will be collected in *options*.
      # We set default values here.
        options = OpenStruct.new

        #If no argument is provided, then it will show the help option.
        ARGV << '-h' if ARGV.empty?
        
        opt_parser = OptionParser.new do |opts|
        opts.banner = "Usage: example.rb [options]"
  
        # Mandatory argument.
        #-i is mandatory to send an array after its called
        opts.on('-i', '--ip-list=IPLIST', Array, 'List of IPs to monitor') do |ipaddr|
            options[:ipaddress] =  ipaddr
            end
                
        #Help options
        opts.on_tail("-h", "--help", "show help") do
            puts "Use '-i' parameter to provide list of IPs separated by a comma"
            puts "Example 10.0.0.0,10.0.0.1"
            exit
            end
    end
  
      opt_parser.parse!(args)
      options
    end 
  end  
 
#Class to set up ssh connection and get server monitoring logs
class ServerMonitors
    def initialize(serverUser,serverIP)
        @user = $USER
        @ip = serverIP
        @key=$KEY
    end

    #Method to start SSH Session, only one session will be opened per instance and needs to be closed when finishing collecting logs.
    def startSession()
        begin
            @session = Net::SSH.start( @ip, @user, :keys => $KEY, :timeout => 5 )
        
        rescue SocketError
            $logger.error  {"Invalid ip. IP Provided " + @ip}
            return 1

        rescue Net::SSH::ConnectionTimeout
            $logger.error  {"SSH Connection timed out for server " + @ip}
            return 1
        
        rescue Net::SSH::AuthenticationFailed
            $logger.error  {"AUTH ERRROR: Authentication failed for user " + @user + " on server " + @ip}
            return 1
        end
    end

    #Method to close SSH Session
    def closeSession()
        @session.close
    end
    
    #Method to use ssh session and run ps -ef and list all running processes
    def listProcesses()
        begin
        $logger.info {"Getting all running processes for server " + @ip }
        Timeout.timeout 10 do
            @output = @session.exec!("ps -ef")
        end

        rescue Timeout::Error
          $logger.error {"Connection succesful, however, command execution timed out for server " + @ip}
    
        else
            $logger.info {"All running processes for server " + @ip + "\n" + @output}
        end
    end

    #Method to use ssh session and get top 3 memory usage processes
    def topMemoryUsage()
        begin
            $logger.info {"Getting top 3 memory usage processes for server " + @ip}
            Timeout.timeout 10 do
                @output = @session.exec!("ps -eo pid,ppid,cmd,%mem --sort=-%mem | head -4")
            end

            rescue Timeout::Error
                $logger.error {"Connection succesful, however, command execution timed out for server " + @ip}
            
            else
                $logger.info {"Top 3 memory usage processes for server " + @ip + "\n" + @output}
            end
    end

    #Method to use ssh session and get top 3 CPU usage processes
    def topCPUUsage()
        begin
            $logger.info {"Getting top 3 CPU usage processes for server " + @ip }
                Timeout.timeout 10 do
                    @output = @session.exec!("ps -eo pid,ppid,cmd,%cpu --sort=-pcpu | head -4")
                end

        rescue Timeout::Error
            $logger.error {"Connection succesful, however, command execution timed out for server " + @ip}
            
        else
            $logger.info {"Top 3 CPU usage processes for server " + @ip + "\n" + @output}
        end
    end

    #Method to use ssh session and get remaining disk capacity
    def diskSpace()
        
        begin
            $logger.info {"Getting disk space for server  " + @ip }
            Timeout.timeout 10 do
                @humanReadable = @session.exec!("df -h")
                @machineReadable = @session.exec!("df")
            end

        rescue Timeout::Error
            $logger.error {"Connection succesful, however, command execution timed out for server " + @ip}
            
        else
            $logger.info {"Disk space on human readable format for server  " + @ip + "\n" + @humanReadable}
            $logger.info {"Disk space on machine readable format for server " + @ip + "\n" + @machineReadable}
            return 0
        end
    end

end

# If to run main only when monitor.rb is called directly from command line.
#  The if will prevent the main to be executed when called by the unit test.
if __FILE__ == $0
    varaibles=LoadVariables.new("config.yml")
    begin
        options = Optparse.parse(ARGV)
        options[:ipaddress].each {|ip|
            server = ServerMonitors.new($USER,ip)
            unless server.startSession() == 1
                server.listProcesses
                server.topMemoryUsage
                server.topCPUUsage
                server.diskSpace
                server.closeSession
            end
        }
    rescue => e
        puts "Error calling Monitor.rb"
        puts "Use '-i' parameter to provide list of IPs separated by a comma"
        puts "ruby monitor.rb -i 10.0.0.0,10.0.0.1"
    end
end
