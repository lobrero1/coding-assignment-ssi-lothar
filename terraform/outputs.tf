output "ec2_global_ips" {
  value = ["${aws_instance.host.*.public_ip}"]
}
