# Coding Assignment SSI Lothar Brenes

## Dependencies
- Ruby version >=2.6.0
- Terraform version >= 0.15.0

## Installation
- Install ruby version 2.7.3 
- See the Ruby Installation page for more detials https://www.ruby-lang.org/en/documentation/installation/
- Install terraform 0.15.1 from https://www.terraform.io/downloads.html
- Go to the project and install the bundler `gem install bundler`
- Install all required gems from Gemfile `bundle install`

## Configuration file - config.yml
- The configuration file allows to set the user that will be used to connect to the instances. This will allow to connect to different OS depending on the preference.
- The configuration file has the relative path for the ssh key to be used.

## Running terraform files
- Terraform directory contains the credentials and backend configuration for the state file. 
- Run the following init command within the terraform directory to configure the backend
```
terraform init --backend-config="access_key=AKIASJDFY3FHHDKNO5AJ" \
--backend-config="secret_key=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"
 ```
- Just run a `terraform apply` to create 3 Amazon Linux instances in AWS.
- Public IPs will be part of the terraform apply output, the idea is to use those ips in the ruby program.
- To Destroy the instances run `terraform destroy`

## Running monitor.rb
- Use '-i' parameter to provide list of IPs separated by a comma.
- Example `ruby monitor.rb -i 10.0.0.0,10.0.0.1`
- If called incorrectly, it will output to STDOUT.

## Monitoring Logs
- Logs from the monitor.rb can be found at monitoring-logs.log
- Errors will be also logged indicating error message.

## Running unit-test-monitors.rb
- You can run the invalid IP and ssh session unit tests by running the following command:
- `ruby unit-test-monitors.rb`
- The unit tests will load the monitor.rb, however, it will not execute the program main during testing.