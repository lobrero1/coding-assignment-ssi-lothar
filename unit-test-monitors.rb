require_relative "monitor"
require "test/unit"
require 'net/ssh/test'
 
class TestSimpleNumber < Test::Unit::TestCase
include Net::SSH::Test
#

def test_invalid_ip
  #Testing that if an invalid IP is provided, then the program returns code 1
   server=ServerMonitors.new("$USER","invalidIP")
   assert_equal(1, server.startSession())
  end

def test_exec_via_ssh_works
  #Testing setting up an ssh sesison and running ps -ef command
    story do |session|
        channel = session.opens_channel
        channel.sends_exec "ps -ef"
        channel.gets_data "result of ps -ef"
        channel.gets_close
        channel.sends_close
      end
  
      assert_scripted do
        result = nil
  
        connection.open_channel do |ch|
          ch.exec("ps -ef") do |success|
            ch.on_data { |c, data| result = data }
            ch.on_close { |c| c.close }
          end
        end
  
        connection.loop
        assert_equal "result of ps -ef", result
      end
    end
end
